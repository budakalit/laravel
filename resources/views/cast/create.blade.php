@extends('layout.master')

@section('judul')
Data Cast
@endsection


@section('content')


<form action='/cast' method="POST">
  @csrf

  <div class="form-group">
    <label >Nama</label>
    <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Lengkap">
  </div>

  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label >Umur</label>
    <input type="number" name="umur" class="form-control" placeholder="Umur">
  </div>
  @error('Umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <textarea name="bio" cols="30" rows="10"></textarea>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection